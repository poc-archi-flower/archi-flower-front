import { Component, OnInit } from '@angular/core';

import { Platform } from '@ionic/angular';
import { Storage } from '@ionic/storage-angular'

import { CartService } from './core/services/cart.service';
import { ToastService } from './core/services/builder/toast.service';
import { Toast } from './core/models/toast.model';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent implements OnInit {

  private _storage: Storage | null = null;
  public isToastOpen: boolean = false;
  public toast: Toast | undefined = undefined;
  public isInitialized: boolean = false;

  constructor(
    private platform: Platform,
    private storage: Storage,
    private cartService: CartService,
    private toastService: ToastService,
  ) {
    this.initializeApp();
  }

  ngOnInit(): void {
    this.initializeApp().then(() => {
      this.cartService.createCart();
      this.toastService.toast$.subscribe((toast: Toast | undefined) => {
        if (toast == undefined) return
        this.toast = toast;
        this.isToastOpen = true;
      })
      this.isInitialized = true;
    })
  }

  async initializeApp() {
    await this.platform.ready();
    this._storage = await this.storage.create();
  }

  public setToastState(state: boolean): void {
    this.isToastOpen = state;
  }
}
