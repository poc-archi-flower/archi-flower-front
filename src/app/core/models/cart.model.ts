import { Flower } from "./flower.model";

export interface Cart {
    id: string;
    items: CartItem[];
}

export interface CartItem {
    id: string;
    flower_id: string;
    quantity: number;
    flower: Flower;
}