export interface Flower {
    id: string;
    name: string;
    description: string;
    price: number;
    color: string;
    create_date: string;
}