import { Flower } from "./flower.model";

export interface Order {
    id: string;
    items: OrderItem[];
    createDate: string;
}

export interface OrderItem {
    id: string;
    quantity: number;
    flower: Flower;
}