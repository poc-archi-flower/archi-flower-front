import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

import { Toast } from '../../models/toast.model';

@Injectable({
  providedIn: 'root'
})
export class ToastService {

  private toastSubject: BehaviorSubject<Toast | undefined> = new BehaviorSubject<Toast | undefined>(undefined);
  public toast$: Observable<Toast | undefined> = this.toastSubject.asObservable();

  constructor() { }

  createToast(toast: Toast): void {
    this.toastSubject.next(toast);
  }
}
