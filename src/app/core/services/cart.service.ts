import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { from, Observable } from 'rxjs';
import { switchMap } from 'rxjs/operators';

import { environment } from 'src/environments/environment';
import { LocalStorageService } from './local-storage.service';
import { Cart } from '../models/cart.model';

@Injectable({
  providedIn: 'root'
})
export class CartService {

  constructor(
    private http: HttpClient,
    private localStorageService: LocalStorageService,
  ) { }

  async createCart(): Promise<void> {
    const cartId = await this.localStorageService.get('cartId');
    if (!cartId) {
      this.http.post<any>(`${environment.api_url}/cart`, []).subscribe((cart) => {
        this.localStorageService.set('cartId', cart?.id);
      });
    }
  }

  getCart(): Observable<any> {
    return from(this.localStorageService.get('cartId')).pipe(
      switchMap(id => {
        if (!id) {
          throw new Error('No cart ID found in local storage');
        }
        return this.http.get<Cart>(`${environment.api_url}/cart/${id}`);
      })
    );
  }

  addItem(flowerId: string, quantity: number): Observable<any> {
    return from(this.localStorageService.get('cartId')).pipe(
      switchMap(cartId => {
        if (!cartId) {
          throw new Error('No cart ID found in local storage');
        }
        return this.http.post<Cart>(`${environment.api_url}/cart/${cartId}`, {
          flower_id: flowerId,
          quantity: quantity
        });
      })
    );
  }

  updateItemQuantity(flowerId: string, quantity: number): Observable<any> {
    return from(this.localStorageService.get('cartId')).pipe(
      switchMap(cartId => {
        if (!cartId) {
          throw new Error('No cart ID found in local storage');
        }
        return this.http.patch<Cart>(`${environment.api_url}/cart/${cartId}`, {
          flower_id: flowerId,
          quantity: quantity
        });
      })
    );
  }

  removeItem(flowerId: string): Observable<any> {
    return from(this.localStorageService.get('cartId')).pipe(
      switchMap(cartId => {
        if (!cartId) {
          throw new Error('No cart ID found in local storage');
        }
        return this.http.delete<Cart>(`${environment.api_url}/cart/${cartId}`, {
          params: {
            flower_id: flowerId
          }
        });
      })
    );
  }

  confirmCart(): Observable<any> {
    return from(this.localStorageService.get('cartId')).pipe(
      switchMap(cartId => {
        if (!cartId) {
          throw new Error('No cart ID found in local storage');
        }
        return this.http.post<any>(`${environment.api_url}/cart/${cartId}/confirm`, {});
      })
    );
  }
}
