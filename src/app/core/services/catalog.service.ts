import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Flower } from '../models/flower.model';
import { Catalog } from '../models/catalog.model';

import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CatalogService {

  constructor(
    private http: HttpClient,
  ) { }

  public getCatalog(page: number = 1, size: number = 10): Observable<Catalog<Flower>> {
    let params = new HttpParams()
      .set('page', page.toString())
      .set('size', size.toString());

    return this.http.get<Catalog<Flower>>(`${environment.api_url}/catalog`, { params });
  }
}
