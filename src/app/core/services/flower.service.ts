import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

import { Flower } from '../models/flower.model';

import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class FlowerService {
  private flowerSource = new BehaviorSubject<Flower | null>(null);
  currentFlower = this.flowerSource.asObservable();

  constructor(private http: HttpClient) {}

  getFlowerCache(id: string): Observable<Flower | null> {
    if (this.flowerSource.getValue() && id === this.flowerSource.getValue()!.id) {
      return this.currentFlower;
    } else {
      return this.getFlowerById(id);
    }
  }

  updateFlowerCache(flower: Flower): void {
    this.flowerSource.next(flower);
  }

  private getFlowerById(id: string): Observable<Flower | null> {
    return this.http.get<Flower>(`${environment.api_url}/flower/${id}`);
  }
}