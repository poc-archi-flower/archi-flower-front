import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UtilsService {
  private darkModeSubject: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  public darkMode$: Observable<boolean> = this.darkModeSubject.asObservable();

  private loadingSubject: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  public loading$: Observable<boolean> = this.loadingSubject.asObservable();

  private menuSubject: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  public menu$: Observable<boolean> = this.menuSubject.asObservable();

  constructor() { 
    this.setDarkMode(window.matchMedia('(prefers-color-scheme: dark)').matches);
  }

  setDarkMode(state:boolean) {
    this.darkModeSubject.next(state);
    document.documentElement.classList.toggle('ion-palette-dark', state);
  }

  setLoading(state:boolean) {
    this.loadingSubject.next(state);
  }

  setMenu(state: boolean) {
    this.menuSubject.next(state);
  }
}