import { Component, OnInit } from '@angular/core';

import { UtilsService } from 'src/app/core/services/utils.service';
import { AccountService } from 'src/app/core/services/account.service';
import { Order, OrderItem } from 'src/app/core/models/order.model';

@Component({
  selector: 'app-account',
  templateUrl: './account.page.html',
  styleUrls: ['./account.page.scss'],
})
export class AccountPage implements OnInit {

  public orders: Order[] | undefined = undefined;

  constructor(
    private utilsService: UtilsService,
    private accountService: AccountService
  ) { }

  ngOnInit() {
    this.utilsService.setLoading(false);
    this.accountService.getOrders().subscribe((orders) => {
      this.orders = orders;
      this.utilsService.setLoading(false);
      console.log(this.orders)
    })
  }

  public handleRefresh(event: any): void {
    this.utilsService.setLoading(false);
    this.accountService.getOrders().subscribe((orders) => {
      this.orders = orders;
      this.utilsService.setLoading(false);
      event.target.complete();
      console.log(this.orders)
    })
  }

  calculateTotal(items: OrderItem[]): number {
    let total = 0;
    items.forEach(item => {
      total += item.quantity * item.flower.price;
    });
    return total;
  }
}
