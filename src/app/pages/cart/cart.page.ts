import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { UtilsService } from 'src/app/core/services/utils.service';
import { CartService } from 'src/app/core/services/cart.service';
import { ToastService } from 'src/app/core/services/builder/toast.service';

import { Cart } from 'src/app/core/models/cart.model';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.page.html',
  styleUrls: ['./cart.page.scss'],
})
export class CartPage implements OnInit {

  public cart: Cart | undefined = undefined;
  public isLoading: boolean = false;

  constructor(
    private utilsService: UtilsService,
    private cartService: CartService,
    private toastService: ToastService,
  ) { }

  ngOnInit() {
    this.utilsService.setLoading(true);
    this.cartService.getCart().subscribe((cart) => {
      this.cart = cart;
      this.utilsService.setLoading(false);
    })
    this.utilsService.loading$.subscribe((state) => { this.isLoading = state; })
  }

  public handleRefresh(event: any): void {
    this.utilsService.setLoading(true);
    this.cartService.getCart().subscribe((cart) => {
      this.cart = cart;
      this.utilsService.setLoading(false);
      event.target.complete();
    })
  }

  increaseQuantity(flowerId: string, quantity: number): void {
    this.cartService.updateItemQuantity(flowerId, quantity + 1).subscribe((cart: Cart) => {
      this.cart = cart;
    })
  }

  decreaseQuantity(flowerId: string, quantity: number): void {
    this.cartService.updateItemQuantity(flowerId, quantity - 1).subscribe((cart: Cart) => {
      this.cart = cart;
    })
  }

  removeFlower(flowerId: string): void {
    this.cartService.removeItem(flowerId).subscribe((cart: Cart) => {
      this.cart = cart;
    })
  }

  price(): number {
    let price: number = 0;
    this.cart?.items.forEach((flower) => {
      price += flower?.flower?.price;
    })
    return price
  }

  confirmCart() {
    this.utilsService.setLoading(true);
    this.cartService.confirmCart().subscribe({
      next: (object) => {
        this.cart = object["cart"]
        this.toastService.createToast({ "message": "La commande est confirmée !", "type": "succes" })
        this.utilsService.setLoading(false);
      },
      error: (error) => {
        this.toastService.createToast({ "message": "Une erreur est survenue pendant la confirmation de la commande, veuillez réessayer ultériement", "type": "danger" })
        this.utilsService.setLoading(false);
      }
    });
  }

}
