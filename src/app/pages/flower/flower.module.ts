import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { FlowerPageRoutingModule } from './flower-routing.module';
import { ReactiveFormsModule } from '@angular/forms';
import { FlowerPage } from './flower.page';

import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    FlowerPageRoutingModule,
    SharedModule,
    ReactiveFormsModule
  ],
  declarations: [FlowerPage]
})
export class FlowerPageModule {}
