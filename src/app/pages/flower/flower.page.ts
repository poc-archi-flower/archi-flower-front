import { Component, OnInit } from '@angular/core';
import { Flower } from 'src/app/core/models/flower.model';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { FlowerService } from 'src/app/core/services/flower.service';
import { UtilsService } from 'src/app/core/services/utils.service';
import { CartService } from 'src/app/core/services/cart.service';
import { ToastService } from 'src/app/core/services/builder/toast.service';

@Component({
  selector: 'app-flower',
  templateUrl: './flower.page.html',
  styleUrls: ['./flower.page.scss'],
})
export class FlowerPage implements OnInit {
  form: FormGroup;

  public flower: Flower | undefined = undefined;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private flowerService: FlowerService,
    private utilsService: UtilsService,
    private cartService: CartService,
    private toastService: ToastService,
    private fb: FormBuilder
  ) { }

  ngOnInit() {
    this.utilsService.setLoading(true);
    const id = this.route.snapshot.paramMap.get('id')!;
    this.flowerService.getFlowerCache(id).subscribe(flower => {
      if (flower == null) this.onFlowerNull();
      this.flower = flower!;
      this.utilsService.setLoading(false);
    });

    this.form = this.fb.group({
      quantity: [1, [Validators.required, Validators.min(1)]]
    });
  }

  get quantityControl() {
    return this.form.get('quantity');
  }

  public handleRefresh(event: any):void {
    this.utilsService.setLoading(true);
    const id = this.route.snapshot.paramMap.get('id')!;
    this.flowerService.getFlowerCache(id).subscribe((flower) => {
      if (flower == null) this.onFlowerNull();
      this.flower = flower!;
      this.utilsService.setLoading(false);
      event.target.complete();
    })
  }

  public onAddToCart(): void {
    if (this.form.invalid) {
      console.log('Form is invalid');
      return;
    }
    const quantity = this.form.value.quantity;
    this.utilsService.setLoading(true);
    this.cartService.addItem(this.flower?.id!, quantity).subscribe({
      next: (result) => {
        this.toastService.createToast({ message: "Article ajouté au panier !", type: "primary" })
        this.utilsService.setLoading(false);
      },
      error: (error) => {
        this.toastService.createToast({ message: "Une erreur a eu lieu, veuillez réessayer ultérieurement.", type: "danger" })
        console.error(error);
        this.utilsService.setLoading(false);
      }
    });
  }

  private onFlowerNull() {
    this.router.navigate(['/']);
  }
}
