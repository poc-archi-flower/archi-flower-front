import { Component, OnInit } from '@angular/core';

import { CatalogService } from 'src/app/core/services/catalog.service';
import { UtilsService } from 'src/app/core/services/utils.service';

import { Catalog } from 'src/app/core/models/catalog.model';
import { Flower } from 'src/app/core/models/flower.model';

@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {

  public catalog: Catalog<Flower>;
  public darkMode: boolean = false;
  public isLoading: boolean = false;

  constructor(
    private catalogService: CatalogService,
    private utilsService: UtilsService,
  ) { }

  ngOnInit() {
    this.utilsService.loading$.subscribe((state) => { this.isLoading = state; })
    this.utilsService.setLoading(true);
    this.utilsService.darkMode$.subscribe(state => this.darkMode = state);
    this.catalogService.getCatalog().subscribe((cat) => {
      this.catalog = cat;
      this.utilsService.setLoading(false);
    })
  }

  public handleRefresh(event: any):void {
    this.utilsService.setLoading(true);
    this.catalogService.getCatalog().subscribe((cat) => {
      this.catalog = cat;
      this.utilsService.setLoading(false);
      event.target.complete();
    })
  }
}
