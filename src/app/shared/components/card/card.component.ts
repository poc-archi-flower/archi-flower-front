import { Component, Input, OnInit } from '@angular/core';
import { Flower } from 'src/app/core/models/flower.model';
import { Router } from '@angular/router';

import { FlowerService } from 'src/app/core/services/flower.service';
import { UtilsService } from 'src/app/core/services/utils.service';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss'],
})
export class CardComponent implements OnInit {

  @Input() flower: Flower;

  constructor(
    private router: Router,
    private flowerService: FlowerService,
    private utilsService: UtilsService
  ) { }

  ngOnInit() { }

  private getFlower(): Flower {
    return this.flower;
  }

  public onCardClick(): void {
    this.utilsService.setLoading(true);
    this.flowerService.updateFlowerCache(this.flower);
    this.router.navigate(['/flower', this.flower.id]);
  }
}


