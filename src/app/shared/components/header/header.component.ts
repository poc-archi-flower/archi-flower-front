import { Component, Input, OnInit } from '@angular/core';
import { MenuController } from '@ionic/angular';
import { Router } from '@angular/router';

import { UtilsService } from 'src/app/core/services/utils.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit {

  @Input() title: string | undefined = "ArchiFlower";
  @Input() showCart: boolean = true;

  public isLoading: boolean = false;
  public darkMode: boolean = false;
  public isHomeRoute: any = null;

  constructor(
    private utilsService: UtilsService,
    private menuCtrl: MenuController,
    private router: Router,
  ) { }

  ngOnInit() {
    this.utilsService.loading$.subscribe(state => this.isLoading = state);
    this.utilsService.darkMode$.subscribe(state => this.darkMode = state);
    this.isHomeRoute = this.router.url === "/";
  }

  public setMode(): void {
    this.utilsService.setDarkMode(!this.darkMode);
  }

  public openCartMenu(): void {
    this.menuCtrl.open('cart-menu');
    this.utilsService.setMenu(true);
  }

  goTo(path: string): void {
    this.router.navigate([`/${path}`]);
  }
}
