import { Component, OnInit } from '@angular/core';
import { MenuController } from '@ionic/angular';
import { Router } from '@angular/router';

import { UtilsService } from 'src/app/core/services/utils.service';
import { CartService } from 'src/app/core/services/cart.service';
import { ToastService } from 'src/app/core/services/builder/toast.service';
import { Cart } from 'src/app/core/models/cart.model';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss'],
})
export class MenuComponent implements OnInit {

  public cart: Cart | undefined = undefined;
  public darkMode: boolean = false;
  public isLoading: boolean = false;

  constructor(
    private utilsService: UtilsService,
    private cartService: CartService,
    private toastService: ToastService,
    private router: Router,
    private menuCtrl: MenuController,
  ) { }

  ngOnInit() {
    this.utilsService.loading$.subscribe((state) => { this.isLoading = state; })
    this.utilsService.menu$.subscribe((state) => {
      this.utilsService.setLoading(true);
      this.utilsService.darkMode$.subscribe(state => this.darkMode = state);
      this.cartService.getCart().subscribe({
        next: (cart) => {
          this.cart = cart;
          this.utilsService.setLoading(false);
        },
        error: (error) => {
          this.toastService.createToast({ message: "Une erreur a eu lieu, veuillez réessayer ultérieurement.", type: "danger" })
          console.error(error);
          this.utilsService.setLoading(false);
        }
      });
    })
  }

  public goTo(path: string): void {
    this.closeCartMenu();
    if (path == 'catalog') {
      this.router.navigate(['/']);
    }
    if (path == 'cart') {
      this.router.navigate(['/cart']);
    }
  }

  public closeCartMenu(): void {
    this.menuCtrl.close('menu');
  }
}
