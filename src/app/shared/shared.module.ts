import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HeaderComponent } from './components/header/header.component';
import { CardComponent } from './components/card/card.component';
import { MenuComponent } from './components/menu/menu.component';

import { IonicModule } from '@ionic/angular';

@NgModule({
  declarations: [
    HeaderComponent,
    CardComponent,
    MenuComponent,
  ],
  imports: [
    CommonModule,
    IonicModule
  ],
  exports: [
    HeaderComponent,
    CardComponent,
    MenuComponent,
  ]
})
export class SharedModule { }
